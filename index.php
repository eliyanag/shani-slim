<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
include 'bootstrap.php';

$app = new \Slim\App;

use ExampleTest\Models\User; 

$app->get('/hello/{name}', function($request, $response,$args){
    return $response->write('Hello '.$args['name']);
}); // navigate to localhost/{foldername}/hello/{name} will show "Hello name on the screen

//READ User - get the users information from database
$app->get('/users', function($request, $response,$args){
    $_user = new User();
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
         $payload[$usr->id] = [
             'id'=>$usr->id,
             'name'=>$usr->name,
             'phone'=>$usr->phone,
             'created_at'=>$usr->created_at,
             'updated_at'=>$usr->updated_at
         ];
    }
    return $response->withStatus(200)->withJson($payload);
 });

 $app->post('/users', function($request, $response,$args){
    $user = $request->getParsedBodyParam('name','');
    $phone = $request->getParsedBodyParam('phone','');
    $token = $request->getParsedBodyParam('token','');
    if($token=='12345'){
    $_user = new user(); //create new user
    $_user->name = $user;
    $_user->phone = $phone;
    $_user->save();
    if($_user->id){
        $payload = ['id'=>$_user->id];
        return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400);
    }
}
   });
//get user
   $app->get('/users/{id}', function($request, $response,$args){    
    $_user = User::find($args['id']);

    $payload=[];

    if($_user->id){
      return $response->withStatus(200)->withJson(json_decode($_user))->withHeader('Access-Control-Allow-Origin', '*');
    }else{
      return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }  
    
});
//Delete
$app->delete('/users/{id}', function($request, $response,$args){
    $user = User::find($args['id']);
    $user->delete();
    
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});
//Update
$app->put('/users/{id}', function($request, $response,$args){
    $name   = $request->getParsedBodyParam('name','');
    $phone  = $request->getParsedBodyParam('phone','');    
    $_user = User::find($args['id']);
    $_user ->name =$name;
    $_user ->phone =$phone;
    if( $_user->save()){
        $payload = ['id'=>$_user->id,"result"=>"The user was updated successfuly"];
        return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});

//Login without JWT
/*$app->post('/login', function($request, $response,$args){
    $name  = $request->getParsedBodyParam('name','');
    $password = $request->getParsedBodyParam('password','');    
    $_user = User::where('name', '=', $name)->where('password', '=', $password)->get();
    
    if($_user[0]->id){
        $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }
});*/

$app->post('/auth', function($request, $response,$args){
    $user = $request->getParsedBodyParam('user', ''); 
    $password = $request->getParsedBodyParam('password', ''); 
    //we need to go to the db, but not now... 
    if($user == 'jack' && $password == '1234'){
    //we need to generate JWT but not now
    $payload = ['token' => '12345'];
    return $response->withStatus(200)->withJson($payload);
    } else {
    $payload = ['token' => null];
    return $response->withStatus(403)->withJson($payload);
    } 
    });
    
    
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$app->run(); //run the application
